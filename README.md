# Unionwells France Systematic Source App

Cutoff switches are used in different conditions and applications in view of their durability, ease to set up, and steady nature of execution. 

A rocker switch is an electrical on/off change that moves starting with one side then onto the next when crushed, leaving the different sides raised and cut down. 

The scaled down switch responds to changes in the environment by opening or closing a lot of variable contacts to restrict the contraption. 

Minimal waterproof switches are applied in load piles, yard cutters, coffee machines, etc 

The basic little switches are applied in ice maker, electric grill, dishwasher, electric warmed water nozzle, etc 

Auto little switches are applied in auto entrance locks, auto reflectors, charging gun, shifters, etc 

The limited scale smaller than usual switches are used in gas broilers, savvy vacuum cleaners, push post motors, etc 

[Unionwells France](https://www.unionwellfrance.com) are a switch creator with over 25 years experience in the switch business. The sum of our switches have UL, ENEC, EK, CQC, etc supports. Our switches are for the most part used in electronic mechanical assemblies, home machines, auto equipment and electrical devices, and the yearly creation limit is more than 300 million pieces of switches.

